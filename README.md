# Foldabilizer

This project holds the C++ implementation of our publication 
"[**Furniture Foldabilizer**](http://honghuali.github.io/projects/foldem/)"
on ACM Transaction on Graphics (SIGGRAPH 2015). 

To compile the code, you need download Qt5 and [starlab](https://github.com/opengp/starlab). 

![](teaser.jpg)

---
We introduce the foldabilization problem for space-saving furniture design. 
Namely, given a 3D object representing a piece of furniture, 
our goal is to apply a minimum amount of modification to the object 
so that it can be folded to save space --- the object is thus foldabilized.
	
We focus on one instance of the problem where folding is with respect to a prescribed folding direction and allowed object modifications include 
hinge insertion and part shrinking.

We develop an automatic algorithm for foldabilization by formulating and solving a
nested optimization problem operating at two granularity levels of the input shape.
	
Specifically, the input shape is first partitioned into a set of integral folding units.
For each unit, we construct a graph which encodes conflict relations, e.g., collisions, 
between foldings implied by various patch foldabilizations within the unit.
	
Finding a minimum-cost foldabilization with a conflict-free folding is an instance of
the maximum-weight independent set problem.
	
In the outer loop of the optimization, we process the folding units in an optimized 
ordering where the units are sorted based on estimated foldabilization costs.
We show numerous foldabilization results computed at interactive speed and
3D-print physical prototypes of these results to demonstrate
manufacturability.