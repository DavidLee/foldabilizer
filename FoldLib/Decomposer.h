#pragma once

#include "FdUtility.h"
#include "UnitScaff.h"

class DecScaff;

class Decomposer
{
public:
	Decomposer(DecScaff* ss);

public:
	// master, slave and relationship
	QVector<PatchNode*> masters;
	QVector<ScaffNode*> slaves;
	QVector< QSet<int> > slave2master;

	// cluster
	QVector< QSet<int> > slaveClusters;

	// more about masters
	PatchNode* baseMaster; // non-virtual master
	QMap<QString, QSet<int> > masterUnitMap;
	QMap<QString, QSet<QString> > masterOrderGreater;
	QMap<QString, QSet<QString> > masterOrderLess;

public:
	bool mergeInterlockUnits();

private:
	// helper
	bool areParallel(QVector<ScaffNode*>& ns);

	// units
	void init();
	void clusterSlaves();
	void createUnits();
	UnitScaff* createUnit(QSet<int> sCluster);
	void computeUnitImportance();

	// master order constraints
	void computeMasterOrderConstraints();
	
private:
	DecScaff* ds;
};

