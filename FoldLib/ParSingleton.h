#pragma once
#include <QObject>
#include "FdUtility.h"

// this is a singleton class to hold all parameters

class ParSingleton : public QObject
{
    Q_OBJECT

public:
	static ParSingleton* instance();
	void restoreDefault();

private:
	// stop the compiler generating methods of copy the object
	ParSingleton(const ParSingleton&) = delete;
	ParSingleton& operator=(const ParSingleton&) = delete;

	// private constructor
	ParSingleton();
	
public:
	// number of keyframes
	int nbKeyframes;

	// squeezing direction
	Vector3 sqzV;

	// aabb constraint
	Vector3 aabbCstrScale;
	Geom::Box aabbCstr;

	// upper bounds of modification
	int maxNbSplits;
	int maxNbChunks;

	// the trade-off weight for splitting
	double splitWeight;

	// the connection ratio
	double connThrRatio;

	// look ahead
	bool lookahead;

	// thickness
	bool useThk;
	bool usePlanA; // folded parts are absolute obstacles
	bool usePlanB; // folded parts cause other patches to be cropped from the bottom
	
	// the speed of chains
	bool uniformH;

	// in place vs. slanted
	bool inPlace;


public:
	// visual options
	enum WHAT2SHOW{ 
		INITIAL_SCAFF, CONF_SCAFF, DECOMP, KEYFRAME
	} what2show;

	bool showAABB;
	bool showCuboid;
	bool showScaffold;
	bool showMesh;

public:
	void setWhat2Show(WHAT2SHOW what);

signals:
	void visualOptionChanged();
	void what2ShowChanged();
	void sqzVChanged();
	void inPlaceChanged();
	void defaultRestored();

public slots:
	void setSqzV(QString sqzV_str);
	void setNbSplits(int N);
	void setNbChunks(int N);
	void setConnThrRatio(double thr);
	void setAabbX(double x);
	void setAabbY(double y);
	void setAabbZ(double z);
	void setCostWeight(double w);
	void setNbKeyframes(int N);
	void setUseThickness(int state);
	void setUsePlanA(int state);
	void setUsePlanB(int state);
	void setUniformH(int state);
	void setInPlace(int state);
	void setLookahead(int state);

	// visual options
	void setWhat2Show(QString what);
	void setShowAABB(int state);
	void setShowCuboid(int state);
	void setShowScaffold(int state);
	void setShowMesh(int state);
};

