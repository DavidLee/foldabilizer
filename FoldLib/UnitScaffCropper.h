#pragma once

#include "UnitScaff.h"

class UnitScaffCropper
{
public:
    UnitScaffCropper(UnitScaff* unit, Vector3 baseOffset);

public:
	QVector<PatchNode*> masters;
	QVector<ScaffNode*> slaves;
	QVector< QVector<QString> > masterPairs;
};


