#include "HChainScaff.h"
#include "Numeric.h"
#include "ParSingleton.h"

HChainScaff::HChainScaff(ScaffNode* slave, PatchNode* base, PatchNode* top)
	:ChainScaff(slave, base, top)
{
}

Geom::Rectangle HChainScaff::getFoldRegion(FoldOption* fn)
{
	Geom::Rectangle base_rect = baseMaster->mPatch;
	Geom::Segment topJointProj = base_rect.getProjection(topJoint);

	double d = rightSeg.length();
	double l = slaveSeg.length();
	double offset = (l - d) / (fn->nSplits + 1);

	Geom::Segment rightSeg, leftSeg;
	if (fn->rightSide)
	{
		leftSeg = topJointProj;
		rightSeg = baseJoint.translated(offset * rightDirect);
	}
	else
	{
		leftSeg = topJointProj.translated(-offset * rightDirect);
		rightSeg = baseJoint;
	}

	// shrink along jointV
	double t0 = fn->position;
	double t1 = t0 + fn->scale;
	leftSeg.cropRange01(t0, t1);
	rightSeg.cropRange01(t0, t1);

	// fold region in 3D
	Geom::Rectangle region(QVector<Vector3>()
		<< leftSeg.P0 << leftSeg.P1
		<< rightSeg.P1 << rightSeg.P0);

	return region;
}

//     left					right
//	   ---						|
//		|step					|
//	   ---						|
//		|step					|d
//	  -----	p0					|
//		|						|
//		|					  -----
//		|						|step
//		|d					   ---
//		|						|step
//		|					   --- p0


void HChainScaff::computeCutPoints(FoldOption* fn)
{
	// the length ratio of two types of segments
	double S = slaveSeg.length();
	double R = rightSeg.length();
	double b = R / S;  // horizontal offset of two joints
	double a = (1 - b) / (fn->nSplits + 1); // zig-zag part

	// offset is at bottom when folding to the right
	double offset = fn->rightSide ? 0 : b;

	// cut points
	cutPoints.clear();
	for (int i = 1; i <= fn->nSplits; i++)
		cutPoints << slaveSeg.getPosition01(offset + i * a);
}

void HChainScaff::fold(double t)
{
	// set up propagation tags
	for(Structure::Node* n : nodes)
		n->removeTag(FIXED_NODE_TAG);	// free all nodes
	baseMaster->addTag(FIXED_NODE_TAG);	// fix base

	// set up hinge angles
	if (ParSingleton::instance()->uniformH)
		foldUniformHeight(t);
	else
		foldUniformAngle(t);

	// restore configuration
	restoreConfiguration();

	// translate the top
	translateTopMaster();
}

void HChainScaff::foldUniformAngle(double t)
{
	// alpha is the root angle, decrease uniformly
	double d = rightSeg.length();
	double sl = slaveSeg.length();
	double alpha = acos(RANGED(0, d / sl, 1)) * (1 - t);

	// compute the angles of other hinges
	int n = chainParts.size();
	double a = (sl - d) / n;
	double b = d + a;
	double bProj = b * cos(alpha);
	double beta;

	// no return : root hinge + 1st hinge
	if (bProj <= d)
	{
		double cos_beta = (d - bProj) / ((n - 1) * a);
		beta = acos(RANGED(0, cos_beta, 1));

		if (foldToRight)
		{
			activeLinks[0]->hinge->angle = M_PI - beta;
			for (int i = 1; i < activeLinks.size() - 1; i++)
				activeLinks[i]->hinge->angle = M_PI;
			activeLinks.last()->hinge->angle = alpha + M_PI - beta;
		}
		else
		{
			activeLinks[0]->hinge->angle = alpha;
			activeLinks[1]->hinge->angle = alpha + M_PI - beta;
			for (int i = 2; i < activeLinks.size(); i++)
				activeLinks[i]->hinge->angle = M_PI;
		}
	}
	// return
	else
	{
		double cos_beta = (b * cos(alpha) - d) / a;
		beta = acos(RANGED(0, cos_beta, 1));

		if (!activeLinks.size()) return;

		if (foldToRight)
		{
			activeLinks[0]->hinge->angle = beta;
			for (int i = 1; i < activeLinks.size() - 1; i++)
				activeLinks[i]->hinge->angle = 2 * beta;
			activeLinks.last()->hinge->angle = alpha + beta;
		}
		else
		{
			activeLinks[0]->hinge->angle = alpha;
			activeLinks[1]->hinge->angle = alpha + beta;
			for (int i = 2; i < activeLinks.size(); i++)
				activeLinks[i]->hinge->angle = 2 * beta;
		}
	}

}

void HChainScaff::foldUniformHeight(double t)
{
	// constant
	int n = chainParts.size();
	double L = slaveSeg.length();
	double d = rightSeg.length();
	double a = (L - d) / n;
	double b = d + a;
	double A = (n - 1) * a;
	double H = upSeg.length();
	double h = (1 - t) * H;

	// phase separator
	computePhaseSeparator();

	// phase-I: no return
	if (h >= heightSep)
	{
		double alpha_orig = acos(RANGED(0, d / L, 1));
		double alpha = alpha_orig;
		if (t > 1e-10)
		{
			TimeInterval alpha_it(angleSep, alpha_orig);
			double x = 2 * b * h;
			double y = 2 * b * d;
			double z = d * d + h * h + b * b - A * A;
			double aa = x * x + y * y;
			double bb = -2 * y * z;
			double cc = z * z - x * x;

			QVector<double> roots = findRoots(aa, bb, cc);
			for (double r : roots){
				alpha = acos(RANGED(0, r, 1));
				if (alpha_it.contains(alpha)) break;
			}
		}

		double cos_beta = (d - b * cos(alpha)) / A;
		double beta = acos(RANGED(0, cos_beta, 1));

		// set angles
		if (foldToRight)
		{
			activeLinks[0]->hinge->angle = M_PI - beta;
			for (int i = 1; i < activeLinks.size() - 1; i++)
				activeLinks[i]->hinge->angle = M_PI;
			activeLinks.last()->hinge->angle = alpha + M_PI - beta;
		}
		else
		{
			activeLinks[0]->hinge->angle = alpha;
			activeLinks[1]->hinge->angle = alpha + M_PI - beta;
			for (int i = 2; i < activeLinks.size(); i++)
				activeLinks[i]->hinge->angle = M_PI;
		}
	}
	// phase-II: return
	else
	{
		double alpha = 0, beta = 0;
		if (t < 1e-10)
		{
			alpha = angleSep;
			double cos_beta = (b * cos(alpha) - d) / a;
			beta = acos(RANGED(0, cos_beta, 1));
		}
		else
		{
			TimeInterval alpha_it(0, angleSep);
			double B = b * b * n * (n - 2);
			double C = -2 * b * d * (n - 1) * (n - 1);
			double D = A * A - h * h - b * b - (n - 1) * (n - 1) * d * d;
			double E = 4 * b * b * h * h - 2 * B * D + C * C;
			double F = -2 * C * D;
			double G = D * D - 4 * b * b * h * h;
			double K = 2 * B * C;

			QVector<double> roots = findRoots(B * B, K, E, F, G);
			for (double r : roots)
			{
				if (r < 0) continue;

				alpha = acos(RANGED(0, r, 1));
				if (alpha_it.contains(alpha))
				{
					double cos_beta = (b * r - d) / a;
					beta = acos(RANGED(0, cos_beta, 1));

					double hh = A * sin(beta) + b * sin(alpha);
					if (fabs(h - hh) < ZERO_TOLERANCE_LOW)
						break;
				}
			}
		}

		// set angles
		if (foldToRight)
		{
			activeLinks[0]->hinge->angle = beta;
			for (int i = 1; i < activeLinks.size() - 1; i++)
				activeLinks[i]->hinge->angle = 2 * beta;
			activeLinks.last()->hinge->angle = alpha + beta;
		}
		else
		{
			activeLinks[0]->hinge->angle = alpha;
			activeLinks[1]->hinge->angle = alpha + beta;
			for (int i = 2; i < activeLinks.size(); i++)
				activeLinks[i]->hinge->angle = 2 * beta;
		}
	}

	return;

	// adjust the position of top master
	Vector3 topPos = upSeg.P0 + h * upSeg.Direction;
	topMaster->translate(topPos - topMaster->center());
}

void HChainScaff::translateTopMaster()
{
	//// adjust the position of top master
	//double ha = a * sin(beta);
	//double hb = b * sin(alpha);
	//double topH = (n - 1) * ha + hb;
	//Vector3 topPos = upSeg.P0 + topH * upSeg.Direction;
	//Vector3 v = topPos - topMaster->center();
	//double delta = dot(v, upSeg.Direction);
	//topMaster->translate(delta * upSeg.Direction);

	// the translation of top master is the same as that of the slave top
	Vector3 newSlaveTop = chainParts.last()->mBox.getPosition(slaveTopCoord);
	Vector3 delta = newSlaveTop - slaveSeg.P1;
	topMaster->translate(topCenterOrig - topMaster->center() + delta);
}

void HChainScaff::computePhaseSeparator()
{
	// constant
	int n = chainParts.size();
	double L = slaveSeg.length();
	double d = rightSeg.length();
	double a = (L - d) / n;
	double b = d + a;
	double A = (n - 1) * a;

	// height
	heightSep = A + sqrt(b * b - d * d);

	// angle
	double sin_angle = (heightSep - A) / b;
	angleSep = asin(RANGED(0, sin_angle, 1));
}

QVector<FoldOption*> HChainScaff::genRegularFoldOptions()
{
	// enumerate all possible combination of nS and nC
	QVector<FoldOption*> options;
	ParSingleton* ps = ParSingleton::instance();
	for (int nS = 1; nS <= ps->maxNbSplits; nS += 2)
	for (int nC = 1; nC <= ps->maxNbChunks; nC++)
		options << ChainScaff::genRegularFoldOptions(nS, nC);

	return options;
}