#pragma once

#include "Scaffold.h"

class Configurer
{
public:
    Configurer(Scaffold* scaff);
    ~Configurer();

private:
	double getConnectThr();
	ScaffNodeArray2D getPerpConnGroups();
	void createMasters();
	void createSlaves();
	void createLinks();

public:
	Scaffold* scaffold;
	QVector<PatchNode*> masters;
	QVector<ScaffNode*> slaves;
};
