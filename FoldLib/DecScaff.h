#pragma once

#include "Scaffold.h"
#include "PatchNode.h"
#include "Numeric.h"
#include "UnitScaff.h"
#include "FoldOptGraph.h"
#include "SuperShapeKf.h"
#include "Decomposer.h"

// DecScaff encodes the hierarchical decomposition of a scaffold

class DecScaff final : public Scaffold
{
public:
    DecScaff(QString id, Scaffold* scaffold);
	~DecScaff();

	// units
	int selUnitIdx;
	QVector<UnitScaff*> units;

	// keyframes of folding
	int keyframeIdx;
	QVector<Scaffold*> keyframes;

private:

	// the decomposer
	Decomposer* decomp;

	// time scale
	double timeScale; // timeScale * unit.duration = normalized time

	// super key frame
	SuperShapeKf* genSuperShapeKf(double t);

	// foldabilization
	double foldabilizeUnit(UnitScaff* unit, double currTime, SuperShapeKf* currKf,
											double& nextTime, SuperShapeKf*& nextKf);
	UnitScaff* getBestNextUnit(double currT, SuperShapeKf* currKeyframe);
	double lookahead(double currT, SuperShapeKf* currKeyframe, QVector<UnitScaff*> ufUnits);

public:
	// key frame
	Scaffold* genKeyframe(double t);

	// store debug info in key frame
	void storeDebugInfo(Scaffold* kf, int uidx);

public:
	// unit selection
	Scaffold* activeScaffold();
	UnitScaff* getSelUnit();
	QStringList getUnitLabels();
	void selectUnit(QString id);

	// foldabilization
	void foldabilize();

	// key frames
	void genKeyframes();
	Scaffold* getSelKeyframe();
	void selectKeyframe(int idx);
};

