#include "UnitScaff.h"
#include "FdUtility.h"
#include "Numeric.h"
#include "ChainScaff.h"
#include "TChainScaff.h"
#include "GeomUtility.h"
#include "ParSingleton.h"
#include "UnitScaffCropper.h"
#include "BundlePatchNode.h"

UnitScaff::UnitScaff(QString id, QVector<PatchNode*>& ms, QVector<ScaffNode*>& ss,
	QVector< QVector<QString> >&) : Scaffold(id)
{
	// clone nodes
	for (PatchNode* m : ms)	{
		masters << (PatchNode*)m->clone();
		Structure::Graph::addNode(masters.last());
	}
	for (ScaffNode* s : ss)
		Structure::Graph::addNode(s->clone());

	// selected chain index
	selChainIdx = -1;

	// importance 
	importance = 0;  

	// plan B
	runningPlanB = false;
	isPlanBSln = false;
	unitCropped = nullptr;
}

void UnitScaff::computeChainImportances()
{
	double totalA = 0;
	for (ChainScaff* c : chains)
	{
		double area = c->getSlaveArea();
		totalA += area;
	}

	for (ChainScaff* c : chains)
		c->importance = c->getSlaveArea() / totalA;
}

UnitScaff::~UnitScaff()
{
	for (ChainScaff* c : chains)
		delete c;
}

void UnitScaff::setImportance(double imp)
{
	importance = imp;
}


Scaffold* UnitScaff::activeScaffold()
{
	Scaffold* selChain = getSelChain();
	if (selChain)  return selChain;
	else		   return this;
}

ChainScaff* UnitScaff::getSelChain()
{
	if (selChainIdx >= 0 && selChainIdx < chains.size())
		return chains[selChainIdx];
	else
		return nullptr;
}

void UnitScaff::selectChain( QString id )
{
	selChainIdx = -1;
	for (int i = 0; i < chains.size(); i++)
	{
		if (chains[i]->mID == id)
		{
			selChainIdx = i;
			break;
		}
	}
}

QStringList UnitScaff::getChainLabels()
{
	QStringList labels;
	for (Scaffold* c : chains)
		labels.push_back(c->mID);

	// append string to select none
	labels << "--none--";

	return labels;
}


double UnitScaff::getNbTopMasters()
{
	return getMasters().size() - 1;
}

double UnitScaff::getTotalSlaveArea()
{
	double a = 0;
	for (ChainScaff* c : chains)a += c->getSlaveArea();
	return a;
}


void UnitScaff::genDebugInfo()
{
	visDebug.clearAll();

	// obstacles
	visDebug.addPoints(getCurrObstacles(), Qt::blue);

	// available fold options/regions
	visDebug.addRectangles(getCurrAFRs(), Qt::green);
}


// The super keyframe is the keyframe + superPatch
// which is an additional patch representing the folded unit
// ***This function is called only if this unit has been foldabilized,
// ***so is getKeyframe(t)
Scaffold* UnitScaff::genSuperKeyframe(double t)
{
	// regular key frame w\o thickness
	Scaffold* keyframe = getKeyframeHub(t);

	// do nothing if the block is NOT fully folded
	if (t < 1) return keyframe;

	// create super patch
	SuperPatchNode* superPatch = new SuperPatchNode(mID + "_super", baseMaster);

	// collect projections of all nodes (including baseMaster) on baseMaster
	Geom::Rectangle base_rect = superPatch->mPatch;
	QVector<Vector2> projPnts2 = base_rect.get2DConners();
	for (ScaffNode* n : keyframe->getScaffNodes())
	{
		if (n->mType == ScaffNode::PATCH)
		{
			Geom::Rectangle part_rect = ((PatchNode*)n)->mPatch;
			projPnts2 << base_rect.get2DRectangle(part_rect).getConners();
		}
		else
		{
			Geom::Segment part_rod = ((RodNode*)n)->mRod;
			projPnts2 << base_rect.getProjCoordinates(part_rod.P0);
			projPnts2 << base_rect.getProjCoordinates(part_rod.P1);
		}
	}

	// resize super patch
	Geom::Rectangle2 aabb2 = Geom::computeAABB(projPnts2);
	superPatch->resize(aabb2);

	// merged parts
	for (ScaffNode* n : keyframe->getScaffNodes())
	{
		// skip virtual edge rod
		if (n->hasTag(EDGE_VIRTUAL_TAG)) continue;

		// tag merged part and encode in the super patch
		n->addTag(MERGED_PART_TAG);
		superPatch->enclosedPartIds << n->mID;
	}

	// add super patch to keyframe
	keyframe->Structure::Graph::addNode(superPatch);

	return keyframe;
}


bool UnitScaff::isExternalPart(ScaffNode* snode)
{
	bool isInternal;
	if (snode->hasTag(SUPER_PATCH_TAG))
	{
		isInternal = false;
		for (QString mm : ((SuperPatchNode*)snode)->enclosedPartIds){
			if (containsNode(mm))
			{
				isInternal = true;
				break;
			}
		}
	}
	// regular part
	else
	{
		isInternal = containsNode(snode->mID);
	}

	return !isInternal;
}

QVector<Vector3> UnitScaff::computeObstaclePnts(SuperShapeKf* ssKeyframe, QString base_mid, QString top_mid)
{
	// obstacle parts
	QVector<ScaffNode*> candidateParts; 
	candidateParts << ssKeyframe->getPartsInbetween(base_mid, top_mid);
	candidateParts << ssKeyframe->getMastersUnrelatedTo(base_mid);
	candidateParts << ssKeyframe->getMastersUnrelatedTo(top_mid);

	ParSingleton* ps = ParSingleton::instance();
	if (ps->usePlanA && ps->useThk)
	{// folded parts are obstacles in plan A with thickness
		candidateParts << ssKeyframe->getFoldedPartsOn(base_mid);
	}

	// external
	QSet<ScaffNode*> obstParts;
	for (ScaffNode* sn : candidateParts){
		if (isExternalPart(sn)) obstParts << sn;
	}

	// sample obstacle parts
	QVector<Vector3> obstPnts;
	for (ScaffNode* obsPart : obstParts){
		obstPnts << obsPart->sampleBoundabyOfScaffold(100);
	}

	return obstPnts;
}

Geom::Rectangle2 UnitScaff::getAabbCstrProj(Geom::Rectangle& base_rect)
{
	Geom::Box aabbCstr = ParSingleton::instance()->aabbCstr;
	int aid = aabbCstr.getAxisId(base_rect.Normal);
	Geom::Rectangle aabbCrossSect = aabbCstr.getCrossSection(aid, 0);

	return base_rect.get2DRectangle(aabbCrossSect);
}

Geom::Rectangle UnitScaff::getBaseRect(SuperShapeKf* ssKeyframe)
{
	return ((PatchNode*)ssKeyframe->getNode(baseMaster->mID))->mPatch;
}

QVector<QString> UnitScaff::getSlnSlaveParts()
{
	QVector<QString> sParts;
	for (ChainScaff* chain : chains)
	{
		for (PatchNode* cs : chain->chainParts)
		{
			sParts << cs->mID;
		}
	}

	return sParts;
}

double UnitScaff::foldabilizePlanA(SuperShapeKf* ssKeyframe, TimeInterval ti)
{
	setRunningPlanB(false);
	double cost = foldabilize(ssKeyframe, ti);
	return cost;
}

double UnitScaff::foldabilizePlanB(SuperShapeKf* ssKeyframe, TimeInterval ti)
{
	// create new unit 
	delete unitCropped;
	Vector3 offset = ssKeyframe->getOffsetOfFoldedPartsOn(baseMaster->mID);
	UnitScaffCropper usc = UnitScaffCropper(this, offset);
	unitCropped = createCroppedUnitScaff(&usc);

	// optimize
	unitCropped->setRunningPlanB(true);
	unitCropped->initFoldSolution();
	double cost = unitCropped->foldabilize(ssKeyframe, ti);
	return cost;
}


double UnitScaff::foldabilizeHub(SuperShapeKf* ssKeyframe, TimeInterval ti)
{
	// plan A
	double costA = foldabilizePlanA(ssKeyframe, ti);
	std::cout << "costA = " << costA << "; \n";

	// plan B
	double costB = maxDouble();
	if (ParSingleton::instance()->usePlanB)
	{
		QVector<ScaffNode*> foldedParts = ssKeyframe->getFoldedPartsOn(baseMaster->mID);
		if (!foldedParts.isEmpty())
		{
			costB = foldabilizePlanB(ssKeyframe, ti);
			std::cout << "costB = " << costB << "\n";
		}
	}

	// compare
	if (costA <= costB)
	{
		isPlanBSln = false;
		return costA;
	}
	else
	{
		isPlanBSln = true;
		return costB;
	}
	
}

Scaffold* UnitScaff::getKeyframeHub(double t)
{
	Scaffold* kf = nullptr;

	if (!isPlanBSln)
	{
		kf = getKeyframe(t);
	}
	else
	{
		// debug
		if (this->mID == "ZU_0")
		{
			int a = 0;
		}


		kf = unitCropped->getKeyframe(t);

		// unwrap the bundle for two reasons:
		// (1) release the real base master for aligning with other units for generating keyframe
		// (2) show the chopped roots of slaves
		kf->unwrapBundleNode(baseMaster->mID, false);

		{
			// debug
			for (Structure::Node* n : kf->nodes)
			{
				if (n->mID == "ground")
				{
					int a = 0;
				}
			}

		}
	}

	return kf;
}

void UnitScaff::setRunningPlanB(bool plan_b)
{
	runningPlanB = plan_b;
}
