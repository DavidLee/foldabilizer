#pragma once

#include <QWidget>
#include "FdPlugin.h"
#include "QListWidgetItem"

namespace Ui {
class FdWidget;
}

class FdWidget : public QWidget
{
    Q_OBJECT

public:
    explicit FdWidget(FdPlugin *fdp, QWidget *parent = 0);
    ~FdWidget();

public:
	Ui::FdWidget *ui;

private:
	FdPlugin *plugin;
	void connectScaffManager();
	void connectParameters();
	void connectFoldManager();
	void connectPlugin();

public slots:
	// default settings
	void updateAllParameters();

	// from Ui
	void selectUnit();
	void selectChain();

	// to Ui
	void setUnitList(QStringList labels);
	void setChainList(QStringList labels);
	void setKeyframeSlider(int N);
	void onKeyframeSliderValueChanged();
	void onKeyframTimeChanged();
	void onWhat2ShowChanged();
	void forceShowKeyFrame();

signals:
	void unitSelectionChanged(QString id);
	void chainSelectionChanged(QString id);
	void keyframeSelectionChanged(int idx);
};

