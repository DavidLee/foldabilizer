#include "FdWidget.h"
#include "ui_FdWidget.h"
#include "ParSingleton.h"

FdWidget::FdWidget(FdPlugin *fdp, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FdWidget)
{
    ui->setupUi(this);
	plugin = fdp;

	// initial parameters
	updateAllParameters();

	// self connections
	this->connect(ui->unitList, SIGNAL(itemSelectionChanged()), SLOT(selectUnit()));
	this->connect(ui->chainList, SIGNAL(itemSelectionChanged()), SLOT(selectChain()));
	this->connect(ui->keyframeSlider, SIGNAL(valueChanged(int)), SLOT(onKeyframeSliderValueChanged()));
	this->connect(ui->keyframeTime, SIGNAL(valueChanged(double)), SLOT(onKeyframTimeChanged()));
	
	// other connections
	connectScaffManager();
	connectParameters();
	connectFoldManager();
	connectPlugin();
}

void FdWidget::updateAllParameters()
{
	ParSingleton* ps = ParSingleton::instance();

	ui->showCuboid->setCheckState(ps->showCuboid ? Qt::Checked : Qt::Unchecked);
	ui->showScaffold->setCheckState(ps->showScaffold ? Qt::Checked : Qt::Unchecked);
	ui->showMesh->setCheckState(ps->showMesh ? Qt::Checked : Qt::Unchecked);
	ui->showAABB->setCheckState(ps->showAABB ? Qt::Checked : Qt::Unchecked);
	ui->what2show->setCurrentIndex(ps->what2show);


	Vector3 sqzV = ps->sqzV;
	if (sqzV.x() == 1) ui->sqzV->setCurrentIndex(0);
	else if (sqzV.x() == -1) ui->sqzV->setCurrentIndex(1);
	else if (sqzV.y() == 1) ui->sqzV->setCurrentIndex(2);
	else if (sqzV.y() == -1) ui->sqzV->setCurrentIndex(3);
	else if (sqzV.z() == 1) ui->sqzV->setCurrentIndex(4);
	else if (sqzV.z() == -1) ui->sqzV->setCurrentIndex(5);
	ui->costWeight->setValue(ps->splitWeight);
	ui->nbSplits->setValue(ps->maxNbSplits);
	ui->nbChunks->setValue(ps->maxNbChunks);

	ui->aabbX->setValue(ps->aabbCstrScale[0]);
	ui->aabbY->setValue(ps->aabbCstrScale[1]);
	ui->aabbZ->setValue(ps->aabbCstrScale[2]);

	ui->nbKeyframes->setValue(ps->nbKeyframes);
	ui->connThrRatio->setValue(ps->connThrRatio);

	ui->lookahead->setCheckState(ps->lookahead ? Qt::Checked : Qt::Unchecked);
	ui->usePlanA->setCheckState(ps->usePlanA ? Qt::Checked : Qt::Unchecked);
	ui->usePlanB->setCheckState(ps->usePlanB ? Qt::Checked : Qt::Unchecked);
	ui->useThk->setCheckState(ps->useThk ? Qt::Checked : Qt::Unchecked);
	ui->inPlace->setCheckState(ps->inPlace ? Qt::Checked : Qt::Unchecked);
}


void FdWidget::connectScaffManager()
{
	// from ui
	plugin->s_manager->connect(ui->fitMethod, SIGNAL(currentIndexChanged(int)), SLOT(setFitMethod(int)));
	plugin->s_manager->connect(ui->createScaffold, SIGNAL(clicked()), SLOT(createScaffold()));
	plugin->s_manager->connect(ui->refitMethod, SIGNAL(currentIndexChanged(int)), SLOT(setRefitMethod(int)));
	plugin->s_manager->connect(ui->fitCuboid, SIGNAL(clicked()), SLOT(refitNodes()));
	plugin->s_manager->connect(ui->changeCuboidType, SIGNAL(clicked()), SLOT(changeNodeType()));
	plugin->s_manager->connect(ui->saveScaffold, SIGNAL(clicked()), SLOT(saveScaffold()));
	plugin->s_manager->connect(ui->loadScaffold, SIGNAL(clicked()), SLOT(loadScaffold()));
}

void FdWidget::connectParameters()
{
	ParSingleton* ps = ParSingleton::instance();

	// update ui
	this->connect(ps, SIGNAL(defaultRestored()), SLOT(updateAllParameters()));
	this->connect(ps, SIGNAL(what2ShowChanged()), SLOT(onWhat2ShowChanged()));

	// visual options
	ps->connect(ui->what2show, SIGNAL(currentIndexChanged(QString)), SLOT(setWhat2Show(QString)));
	ps->connect(ui->showCuboid, SIGNAL(stateChanged(int)), SLOT(setShowCuboid(int)));
	ps->connect(ui->showScaffold, SIGNAL(stateChanged(int)), SLOT(setShowScaffold(int)));
	ps->connect(ui->showMesh, SIGNAL(stateChanged(int)), SLOT(setShowMesh(int)));
	ps->connect(ui->showAABB, SIGNAL(stateChanged(int)), SLOT(setShowAABB(int)));

	// foldabilize parameters
	ps->connect(ui->sqzV, SIGNAL(currentIndexChanged(QString)), SLOT(setSqzV(QString)));
	ps->connect(ui->nbSplits, SIGNAL(valueChanged(int)), SLOT(setNbSplits(int)));
	ps->connect(ui->nbChunks, SIGNAL(valueChanged(int)), SLOT(setNbChunks(int)));
	ps->connect(ui->connThrRatio, SIGNAL(valueChanged(double)), SLOT(setConnThrRatio(double)));
	ps->connect(ui->aabbX, SIGNAL(valueChanged(double)), SLOT(setAabbX(double)));
	ps->connect(ui->aabbY, SIGNAL(valueChanged(double)), SLOT(setAabbY(double)));
	ps->connect(ui->aabbZ, SIGNAL(valueChanged(double)), SLOT(setAabbZ(double)));
	ps->connect(ui->costWeight, SIGNAL(valueChanged(double)), SLOT(setCostWeight(double)));
	ps->connect(ui->nbKeyframes, SIGNAL(valueChanged(int)), SLOT(setNbKeyframes(int)));
	ps->connect(ui->useThk, SIGNAL(stateChanged(int)), SLOT(setUseThickness(int)));
	ps->connect(ui->usePlanA, SIGNAL(stateChanged(int)), SLOT(setUsePlanA(int)));
	ps->connect(ui->usePlanB, SIGNAL(stateChanged(int)), SLOT(setUsePlanB(int)));
	ps->connect(ui->uniformH, SIGNAL(stateChanged(int)), SLOT(setUniformH(int)));
	ps->connect(ui->inPlace, SIGNAL(stateChanged(int)), SLOT(setInPlace(int)));
	ps->connect(ui->lookahead, SIGNAL(stateChanged(int)), SLOT(setLookahead(int)));
}

void FdWidget::connectFoldManager()
{
	// foldabilization
	plugin->f_manager->connect(ui->confScaffold, SIGNAL(clicked()), SLOT(configure()));
	plugin->f_manager->connect(ui->disconnect, SIGNAL(clicked()), SLOT(disconnect()));
	plugin->f_manager->connect(ui->foldabilize, SIGNAL(clicked()), SLOT(foldabilize()));

	// decompose
	plugin->f_manager->connect(ui->decompose, SIGNAL(clicked()), SLOT(decompose()));
	this->connect(plugin->f_manager, SIGNAL(unitsChanged(QStringList)), SLOT(setUnitList(QStringList)));
	this->connect(plugin->f_manager, SIGNAL(chainsChanged(QStringList)), SLOT(setChainList(QStringList)));
	plugin->f_manager->connect(this, SIGNAL(unitSelectionChanged(QString)), SLOT(selectUnit(QString)));
	plugin->f_manager->connect(this, SIGNAL(chainSelectionChanged(QString)), SLOT(selectChain(QString)));

	// key frames
	plugin->f_manager->connect(ui->genKeyframes, SIGNAL(clicked()), SLOT(generateKeyframes()));
	this->connect(plugin->f_manager, SIGNAL(keyframesChanged(int)), SLOT(setKeyframeSlider(int)));

}

void FdWidget::connectPlugin()
{
	// export
	plugin->connect(ui->exportVector, SIGNAL(clicked()), SLOT(exportSVG()));
	plugin->connect(ui->exportPNG, SIGNAL(clicked()), SLOT(exportPNG()));

	plugin->connect(ui->exportCurrent, SIGNAL(clicked()), SLOT(exportCurrent()));
	plugin->connect(ui->exportAllObj, SIGNAL(clicked()), SLOT(exportAllObj()));

	// color
	plugin->connect(ui->assignColor, SIGNAL(clicked()), SLOT(showColorDialog()));
	plugin->connect(ui->colorMasterSlave, SIGNAL(clicked()), SLOT(colorMasterSlave()));

	// snapshot
	plugin->connect(ui->snapshot, SIGNAL(clicked()), SLOT(saveSnapshot()));
	plugin->connect(ui->snapshotAll, SIGNAL(clicked()), SLOT(saveSnapshotAll()));

	// hide
	plugin->connect(ui->hideSelNodes, SIGNAL(clicked()), SLOT(hideSelectedNodes()));
	plugin->connect(ui->unhideAllNodes, SIGNAL(clicked()), SLOT(unhideAllNodes()));

	// test
	plugin->connect(ui->test, SIGNAL(clicked()), SLOT(test()));

}


void FdWidget::forceShowKeyFrame()
{
	ui->what2show->setCurrentIndex(ParSingleton::KEYFRAME);
	ui->keyframeSlider->setValue(0);
}

FdWidget::~FdWidget()
{
    delete ui;
}

void FdWidget::setUnitList( QStringList labels )
{
	ui->unitList->clear();
	ui->unitList->addItems(labels);
}

void FdWidget::setChainList( QStringList labels )
{
	ui->chainList->clear();
	ui->chainList->addItems(labels);
}

void FdWidget::setKeyframeSlider( int N )
{
	ui->keyframeSlider->setMaximum(N-1);
}

void FdWidget::selectUnit()
{
	QList<QListWidgetItem *> selItems = ui->unitList->selectedItems();

	if (!selItems.isEmpty())
	{
		emit(unitSelectionChanged(selItems.front()->text()));
	}
}

void FdWidget::selectChain()
{
	QList<QListWidgetItem *> selItems = ui->chainList->selectedItems();

	if (!selItems.isEmpty())
	{
		emit(chainSelectionChanged(selItems.front()->text()));
	}
}

void FdWidget::onKeyframeSliderValueChanged()
{
	int idx = ui->keyframeSlider->value();
	int N = ui->keyframeSlider->maximum();
	double t = idx / (double)N;

	ui->keyframeTime->blockSignals(true);
	ui->keyframeTime->setValue(t);
	ui->keyframeTime->blockSignals(false);

	plugin->f_manager->selectKeyframe(idx);
}

void FdWidget::onKeyframTimeChanged()
{
	int N = ui->keyframeSlider->maximum();
	double t = ui->keyframeTime->value();
	int idx = (int)(t * N);

	ui->keyframeSlider->blockSignals(true);
	ui->keyframeSlider->setValue(idx);
	ui->keyframeSlider->blockSignals(false);

	plugin->f_manager->selectKeyframe(idx);
}

void FdWidget::onWhat2ShowChanged()
{
	ParSingleton* ps = ParSingleton::instance();
	ui->what2show->setCurrentIndex(ps->what2show);
}

